package es.lajara.escaparates;

import es.lajara.Exeptions.CanNotMergeBrandsException;
import es.lajara.Exeptions.NotExistEnoughtItemException;
import es.lajara.Exeptions.ShowRoomNotOpenException;
import es.lajara.Marca;
import es.lajara.Producto;
import es.lajara.StockProducto;

import java.util.ArrayList;

public class EscaparateTemporal extends Escaparates{

    private int dias;

    private long tienpo;

    public EscaparateTemporal(String nombre, Marca marcar, ArrayList<Producto> productos, int dias) throws CanNotMergeBrandsException {
        super(nombre, marcar,productos);
        this.dias = dias;
        this.tienpo = System.currentTimeMillis();
    }

    public boolean isAbierto(){
        return tienpoRestante() > 0;
    }

    public void consultarTienpoRestantes(){
        long  tienpoRestante = tienpoRestante();
        if (tienpoRestante >= 86400){
            long diasRestantes = tienpoRestante / 86400;
            System.out.println("Quedan: " + diasRestantes + " dias");
        } else {
            long horas = tienpoRestante / 3600;
            long minutos = (tienpoRestante % 3600) / 60;
            long segundos = (tienpoRestante % 3600) % 60;
            System.out.println("Quedan: " + horas + "h " + minutos + "m " + segundos + "s");
        }
    }

    private long tienpoRestante(){
        long diferencia = (System.currentTimeMillis()/1000 - tienpo/1000);
        long diasEnSegundos = dias * 86400;
        return diasEnSegundos - diferencia;
    }

    @Override
    public void compra() throws NotExistEnoughtItemException {
        try {
            comprobarAbierto();
            super.compra();
        }catch (ShowRoomNotOpenException e){
            System.out.println(e.getMessage());
        }

    }

    private void comprobarAbierto() throws  ShowRoomNotOpenException {
        if (!isAbierto()) {
            throw new ShowRoomNotOpenException();
        }
    }
}
