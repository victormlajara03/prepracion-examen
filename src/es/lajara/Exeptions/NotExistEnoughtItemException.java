package es.lajara.Exeptions;

public class NotExistEnoughtItemException extends Exception {
    public NotExistEnoughtItemException(){
        super("No hay suficiente stock");
    }
}
