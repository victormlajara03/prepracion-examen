package es.lajara.escaparates;

import es.lajara.Exeptions.CanNotMergeBrandsException;
import es.lajara.Exeptions.NotExistEnoughtItemException;
import es.lajara.Exeptions.ShowRoomNotOpenException;
import es.lajara.Marca;
import es.lajara.Producto;
import es.lajara.StockProducto;

import java.util.ArrayList;

public class EscaparateMixto extends Escaparates {

    private int dias;

    private long tienpo;

    public EscaparateMixto(String nombre, Marca marcar, ArrayList<Producto> productos) throws CanNotMergeBrandsException {
        super(nombre, marcar,productos);
        this.dias = 3;
        this.tienpo = System.currentTimeMillis();
    }

    @Override
    public void compra() throws NotExistEnoughtItemException {
        try {
            comprobarAbierto();
            super.compra();
        }catch (ShowRoomNotOpenException e){
            System.out.println(e.getMessage());
        }
    }

    public boolean isAbierto(){
        return tienpoRestante() > 0;
    }

    private void comprobarAbierto() throws  ShowRoomNotOpenException {
        if (!hayProductos() || !isAbierto()) {
            throw new ShowRoomNotOpenException();
        }
    }

    private long tienpoRestante(){
        long diferencia = (System.currentTimeMillis()/1000 - tienpo/1000);
        long diasEnSegundos = dias * 86400;
        return diasEnSegundos - diferencia;
    }
}
