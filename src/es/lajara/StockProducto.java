package es.lajara;

import java.util.Random;

public class StockProducto{

    private int cantidad;

    private Producto producto;

    public StockProducto(Producto producto, int cantidad) {
        this.cantidad = cantidad;
        this.producto = producto;
    }

    public int getCantidad() {
        return cantidad;
    }

    public Producto getProducto() {
        return producto;
    }

    public void reducirCantidad(int cantidadComparada){
        this.cantidad -= cantidadComparada;
    }


}
