package es.lajara;

import java.util.Objects;

public class Producto implements Comparable<Producto>{

    private String identificador;

    private float precio;

    private Marca marca;

    public Producto(String identificador, float precio, Marca marca) {
        this.identificador = identificador;
        this.precio = precio;
        this.marca = marca;
    }

    public String getIdentificador() {
        return identificador;
    }

    public Marca getMarca() {
        return marca;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Producto producto = (Producto) o;
        return Objects.equals(identificador, producto.identificador);
    }

    @Override
    public int hashCode() {
        return Objects.hash(identificador);
    }
    
    @Override
    public int compareTo(Producto o) {
        return identificador.compareTo(o.identificador);
    }

    @Override
    public String toString() {
        return "Nombre: " + identificador + " Precio: " + precio + " Marca: " + marca;
    }
}
