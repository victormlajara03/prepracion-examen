package es.lajara.Exeptions;

public class ShowRoomNotOpenException extends Exception{

    public ShowRoomNotOpenException(){
        super("La tienda esta cerrada");
    }
}
