package es.lajara.escaparates;

import es.lajara.Exeptions.CanNotMergeBrandsException;
import es.lajara.Exeptions.NotExistEnoughtItemException;
import es.lajara.Exeptions.ShowRoomNotOpenException;
import es.lajara.GestorIO;
import es.lajara.Marca;
import es.lajara.Producto;
import es.lajara.StockProducto;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;

public abstract class Escaparates {

    private String nombre;

    private Marca marcar;

    private ArrayList<StockProducto> productos;

    public Escaparates(String nombre, Marca marcar, ArrayList<Producto> productos) throws CanNotMergeBrandsException {
        this.nombre = nombre;
        this.marcar = marcar;
        this.productos = new ArrayList<>();
        iniciarArray(productos);
    }

    public void iniciarArray(ArrayList<Producto> listaProductos) throws CanNotMergeBrandsException {
        for (int i = 0; i < listaProductos.size(); i++) {
            if (this.productos.size()<=20) {
                if (!listaProductos.get(i).getMarca().equals(marcar)){
                    throw new CanNotMergeBrandsException();
                }
                this.productos.add(new StockProducto(listaProductos.get(i), cantidadRandom()));
            }
        }
    }

    private int cantidadRandom(){
        Random random = new Random();
        return random.nextInt(10-1+1)+1;
    }

    public void consultarProductos(){
        for (int i = 0; i < productos.size(); i++) {
            if (productos.get(i).getCantidad()>0){
                System.out.println(productos.get(i).getProducto().getIdentificador() + " Cantidad: " + productos.get(i).getCantidad());
            }
        }
    }

    public boolean consultarDisponibilidadStock(Producto productoBuscado, int cantidad){
        for (StockProducto producto : productos) {
            if (producto.getCantidad() >= cantidad && producto.getProducto().equals(productoBuscado)) {
                return true;
            }
        }
        return false;
    }

    public boolean comprobarSiHayProducto(Producto productoBuscado){
        for (StockProducto producto : productos) {
            if (producto.getProducto().equals(productoBuscado) && producto.getCantidad() > 0) {
                return true;
            }
        }
        return false;
    }

    public void compra() throws NotExistEnoughtItemException{
        System.out.println("Selecionar el numero del producto producto");
        for (int i = 0; i < productos.size(); i++) {
            System.out.println(i + " " + productos.get(i).getProducto() + " Cantidad " + productos.get(i).getCantidad());
        }
        int codProducto = GestorIO.getInt();
        System.out.println("Selecione la cantidad");
        int unidades = GestorIO.getInt();
        if (verificarProducto(codProducto)){
            realizarCompra(codProducto,unidades);
        } else {
            System.out.println("El cod del producto no es valido");
        }
    }

    private void realizarCompra(int codigoProducto, int cantidad) throws NotExistEnoughtItemException {
        if (productos.get(codigoProducto).getCantidad() < cantidad) {
            throw new NotExistEnoughtItemException();
        }
        productos.get(codigoProducto).reducirCantidad(cantidad);
        System.out.println("Compra realizada");
    }

    private boolean verificarProducto(int codProducto){
        return codProducto < productos.size() && codProducto >= 0;
    }

    public   boolean hayProductos(){
        for (StockProducto producto : productos) {
            if (producto.getCantidad() > 1) {
                return true;
            }
        }
        return false;
    }

    public void mostrarProductos(){
        ArrayList<Producto> arrayTemporal = crearArraylist();
        Collections.sort(arrayTemporal);
        for (int i = 0; i < arrayTemporal.size(); i++) {
            System.out.println(arrayTemporal.get(i));
        }
    }

    private ArrayList<Producto> crearArraylist(){
        ArrayList<Producto> listaProductos = new ArrayList<>();
        for (int i = 0; i < productos.size(); i++) {
            listaProductos.add(productos.get(i).getProducto());
        }
        return listaProductos;
    }

    public void consultarCantidadProducto(Producto producto){
        for (StockProducto stockProducto : productos) {
            if (producto.equals(stockProducto.getProducto())) {
                System.out.println(stockProducto.getCantidad());
            }
        }
    }

}
