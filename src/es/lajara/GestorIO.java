package es.lajara;

import java.util.Scanner;

public class GestorIO {

    private static Scanner scanner;

    static {
        scanner = new Scanner(System.in);
    }

    public static int getInt() {
        do {
            System.out.println();
            if (!scanner.hasNextInt()) {
                System.out.println("Debe introducir un entero");
                scanner.next();
            } else {
                return scanner.nextInt();
            }
        } while (true);
    }
}

