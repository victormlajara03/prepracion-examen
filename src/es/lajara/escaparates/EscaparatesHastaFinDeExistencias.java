package es.lajara.escaparates;

import es.lajara.Exeptions.CanNotMergeBrandsException;
import es.lajara.Exeptions.NotExistEnoughtItemException;
import es.lajara.Exeptions.ShowRoomNotOpenException;
import es.lajara.Marca;
import es.lajara.Producto;
import es.lajara.StockProducto;

import java.util.ArrayList;

public class EscaparatesHastaFinDeExistencias extends Escaparates{

    private boolean abierto;

    public EscaparatesHastaFinDeExistencias(String nombre, Marca marcar, ArrayList<Producto> productos) throws CanNotMergeBrandsException {
        super(nombre, marcar,productos);
        this.abierto = true;
    }

    @Override
    public void compra() throws NotExistEnoughtItemException{
        try {
            comprobarAbierto();
            super.compra();
        }catch (ShowRoomNotOpenException e){
           System.out.println(e.getMessage());
        }
    }

    private void comprobarAbierto() throws  ShowRoomNotOpenException {
        if (!hayProductos() || !abierto) {
            cerrar();
            throw new ShowRoomNotOpenException();
        }
    }

    public void abrir(){
        abierto = true;
    }

    public void cerrar(){
        abierto = false;
    }
}
