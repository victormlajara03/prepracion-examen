package es.lajara;

import es.lajara.Exeptions.CanNotMergeBrandsException;
import es.lajara.Exeptions.NotExistEnoughtItemException;
import es.lajara.escaparates.EscaparateTemporal;
import es.lajara.escaparates.EscaparatesHastaFinDeExistencias;

import java.util.ArrayList;

public class TestShopOnline {
    public static void main(String[] args) throws NotExistEnoughtItemException, CanNotMergeBrandsException {

        Producto poloRayas = new Producto("polo rayas",50,Marca.LACOSTERA);
        Producto banadorAzul = new Producto("bañador Azul",40,Marca.LACOSTERA);

        ArrayList<Producto> lista1= new ArrayList<>();
        lista1.add(poloRayas);
        lista1.add(banadorAzul);
        EscaparateTemporal escaparateTemporal = new EscaparateTemporal("verano Lacostera  2020",Marca.LACOSTERA,lista1,2);

        escaparateTemporal.mostrarProductos();
        escaparateTemporal.consultarProductos();
        escaparateTemporal.consultarCantidadProducto(poloRayas);
        escaparateTemporal.consultarCantidadProducto(banadorAzul);
        escaparateTemporal.consultarTienpoRestantes();
        escaparateTemporal.compra();
        escaparateTemporal.compra();
        System.out.println(escaparateTemporal.isAbierto());
    }
}
